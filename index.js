const Koa = require('koa')
const app = new Koa()
const Routes = require('./routes');
const utils = require('./utils')();
// const koaBody = require('koa-body');
const http = require('http');

function App() {
    app.use(Routes.routes());
    // app.use(koaBody({
    //     multipart: true,
    //     formidable: {
    //         maxFileSize: 200 * 1024 * 1024    // 设置上传文件大小最大限制，默认2M
    //     }
    // }));
    app.context.utils = utils;
    return app;

}



function onServerReady() {
    let PORT_HTTP = process.env.PORT_HTTP || 1342;
    const app = App();
    // let httpServ = app.listen(PORT_HTTP, '0.0.0.0', () => {
    //     console.log(`Server listening on port: ${PORT_HTTP}`);
    // });
    // // 先不改 timeout , 一樣是 預設為 5000, 要改再說
    // httpServ.keepAliveTimeout = 5000;
    http.createServer(app.callback()).listen(PORT_HTTP);


}

const init = async () => {
    onServerReady();
};


init()