const crypto = require('crypto');

//驗證碼加解密
const captchaKey = {
  sharedSecret: 'Qo5y2RbIECsOpvvujUHsTZKNnMc7O20V',
  captchaAlgorithm: 'bf-ecb',
  encrypt: (text) => {
    let cipher = crypto.createCipher(captchaKey.captchaAlgorithm, captchaKey.sharedSecret);
    let crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
  },
  decrypt: (text) => {
    let decipher = crypto.createDecipher(captchaKey.captchaAlgorithm, captchaKey.sharedSecret);
    let dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
  }
};

//數字補 0
const padLeft = (str, len) => {
  if (!str) throw new Error('參數 str 不正確');
  if (isNaN(len) || len === 0) throw new Error('參數 len 不正確');
  str = str.toString();
  if (str.length >= len)
    return str;
  else
    return padLeft('0' + str, len);
};


function Utils() {

  this.padLeft = padLeft;
  this.captchaKey = captchaKey;
}


module.exports = function() {
  return new Utils();
};