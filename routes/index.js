const Router = require('koa-router');
const router = new Router();

//透過 api 取得驗證碼圖片網址及
router
  .get('/captcha/img', require('./img'))
  .get('/captcha/get', require('./get'));

module.exports = router;