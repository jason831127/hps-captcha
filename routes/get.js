/** TODO::新增 api 文件 captcha/get
 * 076	/captcha/get	取得驗證碼資訊
 * ref: https://docs.google.com/document/d/1Xi6Oc_uaZ8l6Nu-jS9B4zuIff5pzQ3jWLGZ9J5awP4k/edit#
 */
 module.exports = async (ctx, next) => {
    let num = parseInt(Math.random() * 10000);                        //驗證碼最後的內容
    let checkNum = parseInt(Math.random() * 10000);                   //檢核前端傳來的驗證碼 key 用的檢核碼
    let expireTime = Math.ceil(new Date().getTime() / 1000) + 600;    //驗證碼到期時間為當下時間加五分鐘
    //要 encrypt 起來的資料
    let data = {
      expire: expireTime,
      code: ctx.utils.padLeft(num, 4),
      checkCode: ctx.utils.padLeft(checkNum, 4)
    };
    //產生圖片網址
    let n = String(parseInt(data.code, 10) * parseInt(data.checkCode, 10));   //驗證碼最後的內容 x 檢核碼
    let s = data.checkCode;
    let key = '';
    //一串神奇的組合組出給前端的 key
    for (var i = 0; i < s.length; i++) {
      key += s.slice(i, i + 1);
      key += n.slice(i, i + 1);
    }
    key += n.slice(s.length, n.length);

    //圖片網址 query string 直接帶入產稱的這組 key 來生成數字
    let url = ctx.request.url.replace('/captcha/get', '/captcha/img') + `&key=${key}`;
    // 因為前端不想要 /api 開頭，就先加上這個 replace <<<< recommit 多 commit 一絛 
    url = url.replace('/api', ''); 
    let token = ctx.utils.captchaKey.encrypt(JSON.stringify(data)); //encrypt 資料給前端，前端取資料的時候再將這組 key 送給 api
    // var dataDecode = utils.captchaKey.decrypt(token);

    ctx.body = {
      imgUrl: url,
      imgToken: token
    };
    await next();
  };