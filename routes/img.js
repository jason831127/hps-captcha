/** TODO::新增 api 文件 captcha/img
 * 077	/captcha/img	驗證碼圖片網址
 * ref: https://docs.google.com/document/d/1o_60CCYh4JutFdZWh5zwnD6oSO0GN5oKQGMOQgzsPdQ/edit#
 */
 const svgCaptcha = require('svg-captcha');
 const sharp = require('sharp');
 module.exports = async (ctx) => {
 
   let key = ctx.request.query.key;
 
   let check = `${key.slice(0, 1)}${key.slice(2, 3)}${key.slice(4, 5)}${key.slice(6, 7)}`;
   let num = `${key.slice(1, 2)}${key.slice(3, 4)}${key.slice(5, 6)}${key.slice(7, 8)}${key.slice(8, key.length)}`;
   let code = ctx.utils.padLeft(parseInt(num, 10) / parseInt(check, 10), 4);
 
   let opt = {
     size: 4,
     noise: 3,
     color: false,
     width: 130,
     height: 50,
     // backgroud: 'cc9966'
   };
 
   let captcha = svgCaptcha(code, opt);
   let buf = Buffer.from(captcha, 'utf-8');
 
   let jpeg = await sharp(buf).jpeg().flatten({ background: { r: 255, g: 255, b: 255 } }).toBuffer();
   ctx.type = 'image/jpeg';
   ctx.status = 200;
   ctx.body = jpeg;
 
 };